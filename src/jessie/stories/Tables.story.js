import { storiesOf } from '@storybook/vue'

import tables from '../components/tables/Tables.vue'


storiesOf('Samsung', module)
    .add(
        'Tables',
        () => ({
            components: { tables },
            data () {
                return {
                   // show: true
                };
            },
            template: '<tables></tables>'
        }),
        {
            info: 
                `
                 vue-tables-2, bootstrap npm 설치 필요
                `
            ,
        }
    )