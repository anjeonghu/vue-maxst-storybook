import { storiesOf } from '@storybook/vue'

import pagination from '../components/pagination/Pagination.vue'


storiesOf('KT', module)
    .add(
        'Pagination',
        () => ({
            components: { pagination },
            data () {
                return {
                   show: true 
                };
            },
            template: '<pagination :show="show" :totalPageCount="50"></pagination>'
        }),
        {
            info: 
                `
                  ##### <b>Props</b>
                  - <b>show</b> : 페이지네이션 show 여부
                  - <b>totalPageCount</b> : 총 페이지 수
                `
            ,
        }
    )