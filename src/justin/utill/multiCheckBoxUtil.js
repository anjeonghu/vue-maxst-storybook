export default (function () {
  'use strict';
  var __bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };

  return function (parent, child) {
    if (typeof jQuery === 'undefined' && typeof $ === 'undefined') {
      throw new Error('multiCheckBox requires jQuery');
    };
    const MultiCheckBox = function (parent, child) {
      this.child = child;
      this.parent = parent;
      this.init();
    };

    MultiCheckBox.prototype = {
      init: function () {
        $(this.child).each(function () {
          $(this).addClass('single_checkbox');
        });
        this.events();
        this.initAllCheckbox();
      },
      events: function () {
        $(this.parent).click(__bind(function (e) {
          e.stopPropagation();
          let target = e.target;
          this.checkToggleAll(target);
        }, this));

        $(this.child).click(__bind(function (e) {
          e.stopPropagation();
          this.checkToggleSingle();
        }, this))
      },
      initAllCheckbox: function () {
        $(this.parent).removeClass('dash');
        $(this.parent).prop({ 'checked': false });
        $(this.child).each(function () {
          $(this).prop('checked', false);
        });
      },
      checkToggleAll: function (target) {
        let checkBool = $(target).prop('checked');
        $(this.parent).removeClass('dash');
        $(this.child).each(function () {
          $(this).prop('checked', checkBool);
        });
      },
      checkToggleSingle: function () {
        let checkBoxLength = $(this.child).length;
        let selectedLength = $(this.child).filter(':checked').length;
        if (checkBoxLength === selectedLength) {
          $(this.parent).removeClass('dash');
          $(this.parent).prop({
            'checked': true
          })
        } else if (selectedLength === 0) {
          $(this.parent).removeClass('dash');
          $(this.parent).prop({
            'checked': false
          })
        } else {
          $(this.parent).addClass('dash');
        }
      },
      getCheckedLength: function () {
        return $(this.child).filter(':checked').length;
      },
      getCheckedValues: function () {
        let array = [];
        $(this.child).filter(':checked').each(function (idx, ele) {
          let obj = {};
          obj.id = ele.value;
          array.push(obj);
        });
        return array;
      }
    };

    return new MultiCheckBox(parent, child);
  }
})();
