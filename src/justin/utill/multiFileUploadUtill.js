/**
 * Created by front on 2018-08-17.
 */
const MultiFileUpload = (function (element) {
  const MultiFileUpload = function (element) {
    let _ = this;
    let filePicker, fileContainer, multiple, isCanCancel, options;
    isCanCancel = true;
    options = null;

    _.init = function (element, userOption) {
      userOption = userOption || {};

      options = $.extend({
        maxFileSize: 3, // MB 단위
        uploadButton: '.ui-upload-button'
      }, userOption);

      filePicker = $('input[type=file]')[0];
      fileContainer = element;
      _.fileInfo = [];
      _.fileUploadedCount = 0;
      _.fileUploadedFailCount = 0;
      _.fileTotalCount = 0;
      _.fileCanUploadCount = 0;
      _.fileSizeTotal = 0;
      _.invalidFileTotalCount = 0;
      _.fileTotalSize = 0;
      _.invalidMsg = 'invalid filed';
      _.invalidFileList = [];

      let html = '';
      html += "<div class='ui-upload-wrap'>";
      html += "<div class='ui-upload-container' id='ui-upload-container'></div>";
      html += '</div>';
      $(html).css({
        position: 'relative'
      }).appendTo(fileContainer);

      $(filePicker).change(_.fileBindChange);
      $(fileContainer).on('click', '#ui-upload__cancel', _.removeSingleUpload);
    };

    _.fileBindChange = function (event) {
      _.clearAll();
      let files;
      if (filePicker.files.length > 0 && filePicker.files.length <= 100) {
        files = filePicker.files;
        multiple = true;
      } else {
        alert('maximum file attachment  1 ~ 100.');
        return;
      }
      _.onFileSelected(event, {
        files: files,
        multiple: multiple
      });
    };

    _.onFileSelected = function (event, data) {
      $(options.uploadButton).removeAttr('disabled', 'disabled');

      let files;
      if (data && data.files && data.files.length >= 1) {
        files = data.files;

        if (multiple === true) {
          _.uploadMultiple(files);
        }
      }
    };
    _.onUploadComplete = function () {
      // $('.ui-upload-summary__progressbar').css('width', '100%');
      $('.ui-upload-summary__inprogress').text(_.fileUploadedCount + '장의 타깃 이미지 등록이 완료되었습니다! ');
    };
    _.uploadMultiple = function (files) {
      let html = '';
      html += "<table class='table ui-upload-summary__upload-table'>";
      html += "<col width='35%' />";
      html += "<col width='35%' />";
      html += "<col width='20%' />";
      html += "<col width='10%' />";
      html += '</colgroup>';
      html += '<thead>';
      html += '<tr><th>파일명</th><th>등록 가능 여부</th><th>크기</th><th>삭제</th></tr>';
      html += '</thead>';
      html += '<tbody>';
      html += '</tbody>';
      $(html).appendTo('#ui-upload-container');
      html += '<colgroup>';

      for (let i = 0; i < files.length; i++) {
        _.fileTotalCount++;
        if (_.validateFile(files[i], i)) {
          _.fileTotalSize += files[i].size;
          _.fileCanUploadCount++;
        }
        _.createDisplayUpload(files[i], i);
      }
    };

    _.validateFile = function (file, fileId) {
      let type = file.type;
      let size = file.size;
      let fileName = file.name.slice(0, file.name.lastIndexOf('.'));
      if (file !== null) {
        if (!/(image\/gif)|(image\/png)|(image\/jpeg)/gi.test(type)) {
          _.setFileInfo(file, fileId, { errorMsg: '이미지만 등록 가능 합니다.', valid: false });
          _.invalidFileList.push(_.fileInfo[fileId]);
          _.invalidFileTotalCount++;
          return false;
        }
        if (size > (parseInt(options.maxFileSize) * 1024 * 1024)) {
          _.setFileInfo(file, fileId, { errorMsg: '3MB 이하의 파일만 올릴 수 있습니다.', valid: false });
          _.invalidFileList.push(_.fileInfo[fileId]);
          _.invalidFileTotalCount++;
          return false;
        }
        if (!/^([a-zA-Z0-9_]+)$/gi.test(fileName)) {
          _.setFileInfo(file, fileId, { errorMsg: '파일 이름의 형식이 맞지 않습니다.', valid: false });
          _.invalidFileList.push(_.fileInfo[fileId]);
          _.invalidFileTotalCount++;
          return false;
        }
        _.setFileInfo(file, fileId);
      }
      return true;
    };
    _.disableButton = function (isDisable) {
      isDisable || false;
      if (isDisable) {
        $(filePicker).attr({ 'disabled': 'disabled' });
        isCanCancel = false;
      } else {
        $(filePicker).removeAttr('disabled');
        isCanCancel = true;
      }
    };

    _.createDisplayUpload = function (file, fileId) {
      let html = '';
      let fileSize = Number(_.fileInfo[fileId].fileSize / 1024).toFixed(1);
      html += "<tr class='ui-upload-" + fileId + "' id='ui-upload-" + fileId + "');>";
      html += "<td><span class='ui-upload__fileName'><i class='fa fa-image'></i>" + _.fileInfo[fileId].fileName + '</span></td>';
      if (_.fileInfo[fileId].valid === false) {
        html += "<td><span class='ui-upload__error danger'>" + _.fileInfo[fileId].errorMsg + '</span></td>';
      } else {
        html += "<td><span class='ui-upload__error'>" + _.fileInfo[fileId].errorMsg + '</span></td>';
      }
      html += "<td><span class='ui-upload__fileSize'>" + fileSize + 'KB </span></td>';
      if (_.fileInfo[fileId].valid === false) {
        html += "<td><div class='ui-upload__cancel' id='ui-upload__cancel' data-id='" + fileId + "'><img src='/static/media/delete-icon.svg' alt='' role='button'></div></td>";
      } else {
        html += '<td></td>';
      }
      html += '</tr>';

      $(html).appendTo('#ui-upload-container table tbody');

      _.displayFileTotalCount();
    };
    _.displayFileTotalCount = function () {
      let totalFailCount = _.fileUploadedFailCount = _.invalidFileTotalCount;
      let html = '<ul>';
      html += '<li>전체: ' + _.fileTotalCount + '</li>';
      html += '<li>등록 완료: ' + _.fileUploadedCount + '</li>';
      html += '<li>등록 실패: ' + totalFailCount + '</li>';
      html += '</ul>';
      $('.ui-upload-summary__info').html(html);
    };
    _.getFileInfo = function () {
      return _.fileInfo;
    };
    _.setFileInfo = function (file, fileId, extra) {
      let fileName = file.name.slice(0, file.name.lastIndexOf('.'));
      let fileFullName = file.name;
      let fileSize = file.size;
      let key = randomString(30);
      let errorMsg = '등록 가능';
      let status = 'ready';
      let valid = true;

      _.fileInfo[fileId] = {
        key: key,
        fileId: fileId,
        fileName: fileName,
        fileFullName: fileFullName,
        fileSize: fileSize,
        errorMsg: errorMsg,
        status: status,
        data: file,
        base64Data: '',
        valid: valid
      };

      $.extend(_.fileInfo[fileId], extra);
      _.setBase64(file, fileId);
    };
    _.setStatus = function (fileId, extra) {
      if (typeof extra === 'string') {
        _.fileInfo[fileId].status = extra;
      } else if (typeof extra === 'object') {
        extra = extra || {};
        $.extend(_.fileInfo[fileId], extra);
      }

      if (_.fileInfo[fileId].status === 'fail') {
        _.fileInfo[fileId].valid = false;
        _.invalidFileList.push(_.fileInfo[fileId]);
        _.invalidFileTotalCount++;
        _.displayFileTotalCount();
      }

      if (_.fileInfo[fileId].status === 'complete') {
        _.fileUploadedCount++;
        let range = 100 / Number(_.fileTotalCount);
        $('.ui-upload-summary__progressbar').css('width', range * _.fileUploadedCount + '%');
        _.displayFileTotalCount();
      }
    };
    _.removeSingleUpload = function () {
      if (isCanCancel) {
        let fileId = $(this).data('id');

        _.fileTotalCount--;
        _.fileInfo[fileId].status = 'deleted';
        _.invalidFileTotalCount--;

        $('#ui-upload-' + fileId).hide().remove();
        _.displayFileTotalCount();
      }
      return false;
    };
    _.clearAll = function () {
      if (_.fileInfo.length === 0) {
        return;
      }
      _.fileInfo = [];
      _.fileUploadedCount = 0;
      _.fileUploadedFailCount = 0;
      _.fileTotalCount = 0;
      _.fileSizeTotal = 0;
      _.invalidFileTotalCount = 0;
      _.invalidFileList = [];

      _.displayFileTotalCount();
      $('#ui-upload-container').empty();
      $('.ui-upload-summary__info').html('');
      $('.ui-upload-summary__desc').html('');
      $('.ui-upload-summary__progressbar').width(0);
    };
    _.setBase64 = function (file, fileId) {
      let reader = new FileReader();
      let result = '';
      reader.readAsDataURL(file);
      reader.onload = function () {
        result = reader.result;
        _.fileInfo[fileId].base64Data = result.replace(/^data:([a-zA-Z/]*);base64,/g, '');
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };
    };
    _.displaySummary = function (msg) {
      $('.ui-upload-summary__desc').html(msg);
    };
    function randomString (stringLength) {
      let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
      let randomstring, i, rnum;
      for (i = 0; i < stringLength; i++) {
        rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
      }
      return randomstring;
    }
  };
  return new MultiFileUpload();
})();

export default MultiFileUpload;
// initial state
