/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

import Notification from '../components/Notification/Notification.vue'


var message = 'sample message';

storiesOf('Emart', module)
    .add(
        'Notification',
        () => ({
            components: { Notification },
            data () {
                return {
                    message
                };
            },
            template: '<notification :message="message"></notification>'
        }),
        {
            info: {},
        }
    )