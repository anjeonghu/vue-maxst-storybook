/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

import SingleCheckBox from '../components/SingleCheckBox/SingleCheckBox.vue'

storiesOf('Emart', module)
    .add('SingleCheckBox', () => ({
        components: { SingleCheckBox },
        template: '<single-check-box  id="parent" cname="parent"></single-check-box>'
    }));