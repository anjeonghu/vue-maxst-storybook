/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

import SearchBox from '../components/SearchBox/SearchBox.vue'

storiesOf('Emart', module)
    .add(
        'SearchBox',
        () => ({
            components: { SearchBox },
            data () {
                return {
                    types: [{ key :'category', value : '카테고리'}, { key : 'emartCd',  value: '상품 코드'}, { key : 'template',  value: '템플릿'}, { key: 'name', value: '상품명'}],
                    maxlength: 3
                };
            },
            template: '<search-box :types="types" :maxlength=3 @search = ""></search-box>'
        }),
        {
            info: {},
        }
    )