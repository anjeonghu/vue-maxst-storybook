/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

import Modal from '../components/Modal/Modal.vue'

storiesOf('Emart', module)
    .add(
        'Modal',
        () => ({
            components: { Modal },
            template: ' <modal title="title" :show=true  width=395 :showCancelButton = true :showConfirmButton = true :showConfirmText = "로그아웃" :closeOnConfirm = false :closeOnBackground = false @confirmed = "" @close="">'
        }),
        {
            info: {},
        }
    )