/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

import Loading from '../components/Loading/loading.vue'

storiesOf('Emart', module)
    .add(
        'Loading',
        () => ({
            components: { Loading },
            template: ' <loading :show=true></loading>'
        }),
        {
            info: {},
        }
    )