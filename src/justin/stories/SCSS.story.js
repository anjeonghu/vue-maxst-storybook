/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'

storiesOf('Emart', module)
    .add(
        'scss/reset',
        () => ({
            template: '<p></p>'
        }),
        {
            info: `            
                # CSS 초기화 
                    html, body, div, span, applet, object, iframe,
                    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
                    a, abbr, acronym, address, big, cite, code,
                    del, dfn, em, img, ins, kbd, q, s, samp,
                    small, strike, strong, sub, sup, tt, var,
                    b, u, i, center,
                    dl, dt, dd, ol, ul, li,
                    fieldset, form, label, legend,
                    table, caption, tbody, tfoot, thead, tr, th, td,
                    article, aside, canvas, details, embed,
                    figure, figcaption, footer, header, hgroup,
                    menu, nav, output, ruby, section, summary,
                    time, mark, audio, video {
                      margin: 0;
                      padding: 0;
                      border: 0;
                      font-size: 100%;
                      font: inherit;
                      vertical-align: baseline;
                    }
                    /* HTML5 display-role reset for older browsers */
                    article, aside, details, figcaption, figure,
                    footer, header, hgroup, menu, nav, section {
                      display: block;
                    }
                    body {
                      line-height: 1;
                    }
                    ol, ul {
                      list-style: none;
                    }
                    blockquote, q {
                      quotes: none;
                    }
                    blockquote:before, blockquote:after,
                    q:before, q:after {
                      content: '';
                      content: none;
                    }
                    table {
                      border-collapse: collapse;
                      border-spacing: 0;
                    }
                            
            `,
        }
    ).add(
    'scss/setting',
    () => ({
        template: '<p></p>'
    }),
    {
        info: `            
                # SCSS setting
                       // 전역 변수 선언
                       //COLOR
                       $represent-color: #fcb416;
                       $transparent: rgba(255, 255, 255, 0);
                       $white: #fff;
                       $mint: #44ced3;
                       $light-sky: #8adeff;
                       $light-mint: #94e5e1;
                       $black: #2f2f2f;
                       $grey: #ececec;
                       $tab-color: $mint;
                       $danger: #dc3545;
                       // size
                       $font-standard: 14px;
                       
                       // function
                       @function calc-unit-rem ($target) {
                         @return ($target / $font-standard)+rem;
                       }
                       //Mixin
                       @mixin setFontSizeAndLine ($size, $lineHeight, $fontWeight) {
                         font-size: $size + px;
                        line-height: $lineHeight;
                         font-weight: $fontWeight;
                       }
                       @mixin config-font-color($prefix, $colors...) {
                         @each $i in $colors {
                           .#{$prefix}#{nth($i, 1)} {
                             color: nth($i, 2) !important;
                           }
                         }
                       }
                       @mixin config-margin-top($prefix, $margins...) {
                         @each $i in $margins {
                           .#{$prefix}#{nth($i, 1)} {
                             margin-top: nth($i, 2);
                           }
                         }
                       }
                       @mixin config-margin-bottom($prefix, $margins...) {
                         @each $i in $margins {
                           .#{$prefix}#{nth($i, 1)} {
                             margin-bottom: nth($i, 2);
                           }
                         }
                       }
                       @mixin transform($property) {
                         -webkit-transform: $property;
                         -ms-transform: $property;
                         transform: $property;
                       }
                       @mixin transition($property) {
                         -webkit-transition: $property;
                         -ms-transition: $property;
                         transition: $property;
                       }

                            
            `,
    }
)