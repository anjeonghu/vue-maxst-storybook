/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import DraggableList from '../components/DraggableList/DraggableList.vue'


storiesOf('MINT', module)
    .add('DraggableList', () => ({
            components: { DraggableList },
            data () {
                return {
                };
            },
            template: '<draggable-list :items="[{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}]"></draggable-list>'
        }),
        {
            info:
                ` 
                  # 사용 방법                                         
                 `
            ,
        }
    )
