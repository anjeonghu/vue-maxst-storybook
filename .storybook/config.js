/**
 * Created by front on 2018-07-11.
 */
import { configure } from '@storybook/vue';
import 'expose-loader?$!expose-loader?jQuery!jquery'
import { addDecorator } from '@storybook/vue'
import { withInfo } from 'storybook-addon-vue-info'
import fontawesome from '@fortawesome/fontawesome'
addDecorator(withInfo);

import Vue from 'vue';

function loadStories() {
    const req = require.context('../src', true, /\.story\.js$/);
    req.keys().forEach(filename => req(filename));
}


configure(loadStories, module);
